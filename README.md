# MeningMod

Please cite the article [1] if you use the code in your project.

The numerical implementation made in this project permits to compare different models of tumor growth using the mixed-effect approach.  
Four models were studied (Exponential, Gompertz, Linear and Power models) on a new cohort composed of more than 300 meningiomas.
The code was used and tested on the cohort using the "R version 4.2.2 (2022-10-31)" and Monolix 2023R1, Lixoft SAS, a Simulations Plus company. 
The code has not been tested with later versions.

All Copyrights (c) of codes belong to: Virginie Montalibet, 2023, Univ. Bordeaux, CNRS, Inria, IMB, UMR 5251, F-33400 Talence, France.


The project is composed of the following files : 

- ```data_cleaning.R``` provides 2 functions that are used to clean the database and to create a necessary file used by the Monolix algorithm. 
- ```computeModels.R``` provides 2 functions which will compute different models
- ```script_meningiomas.R``` permits to plot and save the  different results obtained. This one is also useful to compare the models tested. 

To test this project, 

- make sure to put the datafile containing your cohort  in the Data Folder (It must contains at least the columns (ID, V0, V1..., T0, T1))
- Then, you can 
    - run the ```main.R``` file (make sure to modify your working directory filepath)

    - Or use the interface found in ```app.R``` associated as follow. 
      - Load the mening.xlsx file 
      - Choose the models you want to compare 
      - Click on Run : 
      - You can then change the Patient you want to look at by selecting a new one
    
   ![title](Images/end.png) 


[1] Evaluation of four tumour growth models to describe the natural history of meningiomas
Engelhardt, Julien et al.
eBioMedicine, Volume 94, 104697


Contact: annabelle.collin@inria.fr, olivier.saut@inria.fr.



